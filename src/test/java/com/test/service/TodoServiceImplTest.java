package com.test.service;

import com.test.dto.TodoPatch;
import com.test.exception.TodoNotFoundException;
import com.test.model.Todo;
import com.test.repository.TodoRepository;
import org.easymock.EasyMock;
import org.junit.Before;
import org.junit.Test;

import static java.util.Optional.empty;
import static java.util.Optional.of;
import static org.easymock.EasyMock.*;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;
import static org.springframework.test.util.ReflectionTestUtils.setField;

public class TodoServiceImplTest {

    private TodoServiceImpl service;
    private TodoRepository repository;

    @Before
    public void setUp() {
        service = new TodoServiceImpl();
        repository = EasyMock.createMock(TodoRepository.class);
        setField(service, "repository", repository);
    }

    @Test
    public void delegatesToRepositoryToCreateTodo() {
        Todo todo = new Todo();
        todo.setText("Test");

        expect(repository.save(todo)).andReturn(todo);

        replay(repository);

        Todo result = service.create(todo);

        verify(repository);

        assertThat(result, equalTo(todo));
    }

    @Test
    public void delegatesToRepositoryToFindTodo() {
        Todo todo = new Todo();
        todo.setText("Test");

        Long id = 1L;

        expect(repository.findById(id)).andReturn(of(todo));

        replay(repository);

        Todo result = service.getTodo(id);

        verify(repository);

        assertThat(result, equalTo(todo));
    }

    @Test(expected = TodoNotFoundException.class)
    public void getThrowsExceptionWhenNotFound() {
        Long id = 1L;

        expect(repository.findById(id)).andReturn(empty());

        replay(repository);

        try {
            service.getTodo(id);
            fail("Should have thrown exception!");
        } finally {
            verify(repository);
        }
    }

    @Test
    public void updateRetrievesTodoAndAppliesChange() {
        Long id = 1L;
        String updatedText = "Finished";
        boolean updatedStatus = true;

        Todo existing = createMock(Todo.class);

        TodoPatch update = new TodoPatch();
        update.setText(updatedText);
        update.setCompleted(updatedStatus);

        expect(repository.findById(id)).andReturn(of(existing));
        existing.setText(updatedText);
        existing.setCompleted(updatedStatus);

        expect(repository.save(existing)).andReturn(new Todo());

        replay(repository, existing);

        service.updateTodo(id, update);

        verify(repository, existing);
    }

    @Test(expected = TodoNotFoundException.class)
    public void updateThrowsExceptionWhenNotFound() {
        Long id = 1L;

        expect(repository.findById(id)).andReturn(empty());

        replay(repository);

        try {
            TodoPatch request = new TodoPatch();
            service.updateTodo(id, request);
            fail("Should have thrown exception!");
        } finally {
            verify(repository);
        }
    }

}