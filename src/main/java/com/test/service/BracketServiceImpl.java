package com.test.service;

import org.springframework.stereotype.Service;

import java.util.Stack;

@Service
public class BracketServiceImpl implements BracketService {

    @Override
    public boolean validateBrackets(String input) {
        Stack<Character> bracketStack = new Stack<>();

        for (int i = 0; i < input.length(); i++){
            char character = input.charAt(i);

            if (isOpenBracket(character)) {
                bracketStack.push(character);
                continue;
            }

            if (isCloseBracket(character)) {
                if (bracketStack.isEmpty()) {
                    return false;
                }

                if (bracketsMatch(character, bracketStack.peek())) {
                    bracketStack.pop();
                } else {
                    return false;
                }
            }
        }
        return bracketStack.isEmpty();
    }

    private boolean bracketsMatch(char closingBracket, char openingBracket) {
        return closingBracket == ')' && openingBracket == '(' ||
                closingBracket == '}' && openingBracket == '{' ||
                closingBracket ==']' && openingBracket == '[';
    }

    private boolean isCloseBracket(char character) {
        return character == ')' || character == '}' || character == ']';
    }

    private boolean isOpenBracket(char character) {
        return character == '(' || character == '{' || character == '[';
    }

}
