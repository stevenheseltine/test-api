package com.test.controller;

import com.test.dto.ValidateBracketsResponse;
import com.test.service.BracketService;
import org.junit.Before;
import org.junit.Test;

import static org.easymock.EasyMock.*;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.springframework.test.util.ReflectionTestUtils.setField;

public class TaskControllerTest {

    private BracketService bracketService;
    private TaskController controller;

    @Before
    public void setUp() {
        bracketService = createMock(BracketService.class);

        controller = new TaskController();

        setField(controller, "bracketService", bracketService);
    }

    @Test
    public void validateBracketsDelegatesToBracketValidator() {
        String request = "input";
        ValidateBracketsResponse expectedResponse = new ValidateBracketsResponse(request, false);
        expect(bracketService.validateBrackets(request)).andReturn(false);

        replay(bracketService);

        assertThat(controller.validateBrackets(request), equalTo(expectedResponse));

        verify(bracketService);
    }
}