package com.test.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.Size;

public class TodoPatch {
    @Size(min=1, max=50, message="Must be between 1 and 50 chars long")
    private String text;

    private Boolean completed;
    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    @JsonProperty("isCompleted")
    public Boolean isCompleted() {
        return completed;
    }

    public void setCompleted(boolean completed) {
        this.completed = completed;
    }
}
