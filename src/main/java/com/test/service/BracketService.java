package com.test.service;

public interface BracketService {
    boolean validateBrackets(String input);
}
