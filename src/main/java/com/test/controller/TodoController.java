package com.test.controller;

import com.test.dto.TodoPatch;
import com.test.model.Todo;
import com.test.service.TodoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
public class TodoController {

    @Autowired
    private TodoService todoService;

    @PostMapping(path = "/todo")
    Todo createTask(@Valid @RequestBody Todo todo) {
        return todoService.create(todo);
    }

    @GetMapping(path = "/todo/{id}")
    public Todo getTodo(@PathVariable Long id) {
        return todoService.getTodo(id);
    }

    @PatchMapping(path = "/todo/{id}")
    public Todo updateTodo(@PathVariable Long id, @Valid @RequestBody TodoPatch todo) {
        return todoService.updateTodo(id, todo);
    }
}
