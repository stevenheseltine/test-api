package com.test.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import static org.apache.commons.lang3.builder.EqualsBuilder.reflectionEquals;
import static org.apache.commons.lang3.builder.HashCodeBuilder.reflectionHashCode;

public class ValidateBracketsResponse {
    private final boolean result;
    private String input;

    public ValidateBracketsResponse(String input, boolean result) {
        this.input = input;
        this.result = result;
    }

    public String getInput() {
        return input;
    }

    @JsonProperty("isBalanced")
    public boolean getResult() {
        return result;
    }

    @Override
    public int hashCode() {
        return reflectionHashCode(this);
    }

    @Override
    public boolean equals(Object obj) {
        return reflectionEquals(this, obj);
    }



}
