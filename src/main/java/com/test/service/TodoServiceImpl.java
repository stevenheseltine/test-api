package com.test.service;

import com.test.dto.TodoPatch;
import com.test.exception.TodoNotFoundException;
import com.test.model.Todo;
import com.test.repository.TodoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class TodoServiceImpl implements TodoService {

    @Autowired
    private TodoRepository repository;

    @Override
    public Todo create(Todo todo) {
        return repository.save(todo);
    }

    @Override
    public Todo getTodo(Long id) {
        Optional<Todo> todo = repository.findById(id);
        if (!todo.isPresent()) {
            throw new TodoNotFoundException();
        }
        return todo.get();
    }

    @Override
    public Todo updateTodo(Long id, TodoPatch patch) {
        Optional<Todo> existing = repository.findById(id);
        if (!existing.isPresent()) {
            throw new TodoNotFoundException();
        }
        Todo todo = existing.get();

        if (patch.getText() != null) {
            todo.setText(patch.getText());
        }

        if (patch.isCompleted() != null) {
            todo.setCompleted(patch.isCompleted());
        }

        return repository.save(todo);
    }

}
