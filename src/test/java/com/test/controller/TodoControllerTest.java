package com.test.controller;

import com.test.dto.TodoPatch;
import com.test.model.Todo;
import com.test.service.TodoService;
import org.easymock.EasyMock;
import org.junit.Before;
import org.junit.Test;

import static org.easymock.EasyMock.*;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.springframework.test.util.ReflectionTestUtils.setField;

public class TodoControllerTest {

    private TodoService todoService;
    private TodoController controller;

    @Before
    public void setUp() {
        todoService = EasyMock.createMock(TodoService.class);
        controller = new TodoController();
        setField(controller, "todoService", todoService);

    }

    @Test
    public void createTodoDelegatesToService() {
        Todo request = createTodo("test");
        Todo expected = createTodo("test");

        expect(todoService.create(request)).andReturn(expected);

        replay(todoService);

        assertThat(controller.createTask(request), equalTo(expected));

        verify(todoService);
    }

    @Test
    public void getTodoDelegatesToService() {
        Todo expected = createTodo("test");

        Long id = 1L;

        expect(todoService.getTodo(id)).andReturn(expected);

        replay(todoService);

        assertThat(controller.getTodo(id), equalTo(expected));

        verify(todoService);
    }

    @Test
    public void updateDelegatesToService() {
        Todo expected = createTodo("test");
        TodoPatch request = new TodoPatch();
        Long id = 1L;

        expect(todoService.updateTodo(id, request)).andReturn(expected);

        replay(todoService);

        assertThat(controller.updateTodo(id, request), equalTo(expected));

        verify(todoService);
    }

    private Todo createTodo(String text) {
        Todo todo = new Todo();
        todo.setText(text);
        return todo;
    }
}