package com.test.model;

import com.test.dto.ValidateBracketsResponse;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class ValidateBracketsResponseTest {

    @Test
    public void canCreateResult() {
        String input = "input";
        boolean result = true;

        ValidateBracketsResponse validateBracketsResponse = new ValidateBracketsResponse(input, result);

        assertThat(validateBracketsResponse.getInput(), is(equalTo(input)));
        assertThat(validateBracketsResponse.getResult(), is(equalTo(result)));
    }
}