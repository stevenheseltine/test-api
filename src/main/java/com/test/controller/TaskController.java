package com.test.controller;

import com.test.dto.ValidateBracketsResponse;
import com.test.service.BracketService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TaskController {

    @Autowired
    private BracketService bracketService;

    @GetMapping(path = "/tasks/validateBrackets", produces = "application/json")
    public ValidateBracketsResponse validateBrackets(@RequestParam String input) {
        return new ValidateBracketsResponse(input, bracketService.validateBrackets(input));
    }

}
