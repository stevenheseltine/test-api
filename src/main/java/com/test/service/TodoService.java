package com.test.service;

import com.test.dto.TodoPatch;
import com.test.model.Todo;

public interface TodoService {
    Todo create(Todo request);
    Todo getTodo(Long id);
    Todo updateTodo(Long id, TodoPatch todo);
}
