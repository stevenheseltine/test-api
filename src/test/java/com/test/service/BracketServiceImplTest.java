package com.test.service;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class BracketServiceImplTest {

    private BracketService bracketService;
    @Before
    public void setUp() {
        bracketService = new BracketServiceImpl();
    }

    @Test
    public void returnsTrueForBalancedBrackets() {
        assertTrue(bracketService.validateBrackets("()"));
        assertTrue(bracketService.validateBrackets("( )"));
        assertTrue(bracketService.validateBrackets("[]"));
        assertTrue(bracketService.validateBrackets("[ ]"));
        assertTrue(bracketService.validateBrackets("{}"));
        assertTrue(bracketService.validateBrackets("{ }"));
        assertTrue(bracketService.validateBrackets("([])"));
        assertTrue(bracketService.validateBrackets("( { } )"));
        assertTrue(bracketService.validateBrackets("[{()}]"));
        assertTrue(bracketService.validateBrackets("[hello]"));
        assertTrue(bracketService.validateBrackets("{hello{world}[!(!)]}"));
    }

    @Test
    public void returnsFalseForUnbalancedBrackets() {
        assertFalse(bracketService.validateBrackets("(hello"));
        assertFalse(bracketService.validateBrackets("world{})"));
        assertFalse(bracketService.validateBrackets("[[]"));
        assertFalse(bracketService.validateBrackets("[[}]"));
        assertFalse(bracketService.validateBrackets("{(])}"));
    }

}